﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameCollegeExamen;

namespace testMobile
{
  [TestClass]
  public class UnitTest1
  {
    private Student testStudent;
    private Student testStudent2;

    [TestInitialize]
    public void InitialiseerStudent()
    {
      testStudent = new Student("Jefke Peeters");
      testStudent2 = new Student("Maria Peeters");
    }


    [TestMethod]
    public void TestStem()
    {
      testStudent.Stem();
      int stem1 = testStudent.AantalStemmen;
      testStudent.Stem();
      int stem2 = testStudent.AantalStemmen;
      Assert.AreNotEqual(stem1, stem2);

    }
    [TestMethod]
    public void TestStemmen()
    {

      for (int i = 0; i < 100; i++)
      {
        testStudent.Stem();
        int stem1 = testStudent.AantalStemmen;
        testStudent.Stem();
        int stem2 = testStudent.AantalStemmen;

        Assert.AreEqual(stem1, stem2);
      }
    }

    [TestMethod]
    public void TestGlobaalStemmen()
    {

      for (int i = 0; i < 100; i++)
      {
        testStudent.Stem();
        testStudent.Stem();
        testStudent.Stem();
        int stemStudent2 = testStudent2.AantalStemmen;
        testStudent2.Stem();
        int stemStudent3 = testStudent2.AantalStemmen;

        Assert.AreEqual(stemStudent2, stemStudent3);
      }
    }


  }
}
