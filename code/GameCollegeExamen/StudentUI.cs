﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollegeExamen
{
  public partial class StudentUI : UserControl
  {
    private Student model;
    private StudentController controller;

    public StudentUI( Student sModel, StudentController sController)
    {
      InitializeComponent();
      this.model = sModel;
      this.controller = sController;
      lblNaamStudent.Text = model.Naam;
    }

    private void btnStem_Click(object sender, EventArgs e)
    {
      controller.BttnStemClicked();
      UpdateStudentUI();
    }

    public void UpdateStudentUI()
    {
      lblAantalStemmen.Text = Convert.ToString(model.AantalStemmen);
    }
  }
}
