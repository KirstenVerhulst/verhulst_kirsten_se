﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class Mobile
  {
    public const int MAX_AANTAL_STEMMEN = 3;
    public const int AANTAL_DEMOS = 5;

    private GameDemo[] gameDemos = new GameDemo[AANTAL_DEMOS];

    public Mobile()
    { 
    gameDemos[0] = new GameDemo("Game Design");
    gameDemos[1] = new GameDemo("Programming fundamentals");
    gameDemos[2] = new GameDemo("Character Creation");
    gameDemos[3] = new GameDemo("Unity");
    gameDemos[4] = new GameDemo("3D Animation");
    }

    public GameDemo GetGameDemo(int index)
    {
      return gameDemos[index];
    }

    }
  }

