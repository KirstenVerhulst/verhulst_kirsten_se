﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class StudentController
  {
    private Student model;
    public StudentController(Student sModel)
    {
      this.model = sModel;
    }

    public StudentUI GetStudentUI(Student studentModel) 
    {
      StudentUI studentUI = new StudentUI(studentModel, this);
      return studentUI;
    }

    public void BttnStemClicked()
    {
      model.Stem();
    }

  }
}
