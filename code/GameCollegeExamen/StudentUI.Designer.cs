﻿namespace GameCollegeExamen
{
  partial class StudentUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblAantalStemmen = new System.Windows.Forms.Label();
      this.lblstem = new System.Windows.Forms.Label();
      this.lblNaamStudent = new System.Windows.Forms.Label();
      this.btnStem = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lblAantalStemmen
      // 
      this.lblAantalStemmen.AutoSize = true;
      this.lblAantalStemmen.Location = new System.Drawing.Point(97, 32);
      this.lblAantalStemmen.Name = "lblAantalStemmen";
      this.lblAantalStemmen.Size = new System.Drawing.Size(13, 13);
      this.lblAantalStemmen.TabIndex = 7;
      this.lblAantalStemmen.Text = "0";
      // 
      // lblstem
      // 
      this.lblstem.AutoSize = true;
      this.lblstem.Location = new System.Drawing.Point(3, 32);
      this.lblstem.Name = "lblstem";
      this.lblstem.Size = new System.Drawing.Size(87, 13);
      this.lblstem.TabIndex = 6;
      this.lblstem.Text = "Aantal Stemmen:";
      // 
      // lblNaamStudent
      // 
      this.lblNaamStudent.AutoSize = true;
      this.lblNaamStudent.Location = new System.Drawing.Point(3, 7);
      this.lblNaamStudent.Name = "lblNaamStudent";
      this.lblNaamStudent.Size = new System.Drawing.Size(75, 13);
      this.lblNaamStudent.TabIndex = 5;
      this.lblNaamStudent.Text = "Naam Student";
      // 
      // btnStem
      // 
      this.btnStem.Location = new System.Drawing.Point(35, 48);
      this.btnStem.Name = "btnStem";
      this.btnStem.Size = new System.Drawing.Size(75, 23);
      this.btnStem.TabIndex = 4;
      this.btnStem.Text = "Stem";
      this.btnStem.UseVisualStyleBackColor = true;
      this.btnStem.Click += new System.EventHandler(this.btnStem_Click);
      // 
      // StudentUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblAantalStemmen);
      this.Controls.Add(this.lblstem);
      this.Controls.Add(this.lblNaamStudent);
      this.Controls.Add(this.btnStem);
      this.Name = "StudentUI";
      this.Size = new System.Drawing.Size(118, 78);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblAantalStemmen;
    private System.Windows.Forms.Label lblstem;
    private System.Windows.Forms.Label lblNaamStudent;
    private System.Windows.Forms.Button btnStem;
  }
}
