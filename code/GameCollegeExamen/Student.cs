﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class Student
  {
    private int aantalStemmen;
    private string naamStudent;

    public Student(string naam)
    { 
      aantalStemmen = 0;
      naamStudent = naam;
    }

    public void Stem()
    {
      if (GameDemo.stemmenOver > 0)
      {
        GameDemo.stemmenOver--;
        aantalStemmen++;
      }
 
    }

    public int AantalStemmen
    { get { return aantalStemmen; } }

    public string Naam { get {return naamStudent;} }
  }
}
