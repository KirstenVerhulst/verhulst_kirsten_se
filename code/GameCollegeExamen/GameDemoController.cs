﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class GameDemoController
  {
    private GameDemo model;
    private GameDemoUI view;


    private List<StudentController> studentControllers = new List<StudentController>();
   
    public GameDemoController(GameDemo gGameDemo)
    {
      model = gGameDemo;
    }

    public GameDemoUI GetGameDemoUI()
    {
      this.view = new GameDemoUI(model, this);
      return view;
    }

    public StudentUI GetStudentUI(int index)
    {
      Student s = model.GetStudent(index); 
      studentControllers.Add(new StudentController(s));
      return studentControllers[index].GetStudentUI(s);
    }

    public void BttnGamedemoClicked()
    {
      this.model.Clicked();
      view.UpdateGameDemoUI();
    }
  }
}
