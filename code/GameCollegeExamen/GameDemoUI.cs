﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollegeExamen
{
  public partial class GameDemoUI : UserControl
  {
    private GameDemo model;
    private GameDemoController controller;
    private StudentUI[] studentUIs = new StudentUI[5];

    public GameDemoUI(GameDemo gModel, GameDemoController gController)
    {
      InitializeComponent();
      this.model = gModel;
      this.controller = gController;
      btnDemo.Text = model.DemoNaam;

    }



    public void btnDemo_Click(object sender, EventArgs e)
    {
      controller.BttnGamedemoClicked();

      if (model.IsClicked)
      {
        for (int i = 0; i < GameDemo.AANTAL_STUDENTEN; i++)
        {
          studentUIs[i] = this.controller.GetStudentUI(i);
          studentUIs[i].Location = new System.Drawing.Point(10, 55 + i * 110);
          this.Controls.Add(studentUIs[i]);
          studentUIs[i].UpdateStudentUI();
        }
      }

    }

    public void UpdateGameDemoUI()
    {
      if (model.IsClicked)
      {
        btnDemo.BackColor = Color.WhiteSmoke;
      }
      else
      {
        btnDemo.BackColor = Color.DarkGray;

        for (int i = 0; i < GameDemo.AANTAL_STUDENTEN; i++)
        {
          this.Controls.Remove(studentUIs[i]);
        }
      }

      btnDemo.Text = model.DemoNaam;
    }
  }
}
