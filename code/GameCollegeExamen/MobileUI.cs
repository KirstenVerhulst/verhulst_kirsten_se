﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollegeExamen
{
  public partial class MobileUI : UserControl
  {
    private Mobile model;
    private MobileController controller;
    private GameDemoUI[] gameDemoUIs = new GameDemoUI[Mobile.AANTAL_DEMOS]; //CONSTANTE IN APPLICATION ZETTEN EN GEBRUIKEN



    public MobileUI(Mobile mModel, MobileController mController)
    {
      InitializeComponent();
      this.model = mModel;
      this.controller = mController;
      UpdateMobileUI();

      for (int i = 0; i < Mobile.AANTAL_DEMOS; i++)
      {
        gameDemoUIs[i] = this.controller.GetGameDemoUI(i);
        gameDemoUIs[i].Location = new System.Drawing.Point(10 + i * 150, 10);
        this.Controls.Add(gameDemoUIs[i]);

      }
    }

    public void UpdateMobileUI() 
    {
      int aantal = GameDemo.stemmenOver;
      lblAantalstemmen.Text = Convert.ToString(aantal);
    }

  }
}
