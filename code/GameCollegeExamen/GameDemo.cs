﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class GameDemo
  {
    public const int AANTAL_STUDENTEN = 5;
    public static int stemmenOver = 3;

    private Student[] students = new Student[AANTAL_STUDENTEN];
    private bool clicked = false;
    private string demoNaam;

    public GameDemo(string naam)
    { 
    students[0] = new Student("Stijn Op de Beeck");
    students[1] = new Student("Lisa van Steen");
    students[2] = new Student("Laurens pooters");
    students[3] = new Student("Oscar Van der Velden");
    students[4] = new Student("Sophie Callenaere");

    demoNaam = naam;
    }


    public void Clicked() 
    {
      if (!clicked)
      {
        clicked = true;
      }
      else
      {
        clicked = false;
      }
    }

    public Student GetStudent(int index)
    { 
      return students[index]; 
    }

    public bool IsClicked { get { return clicked; } set { clicked = value; } }

    public string DemoNaam { get { return demoNaam;} }
  }
}
