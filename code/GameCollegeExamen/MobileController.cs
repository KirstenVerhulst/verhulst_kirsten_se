﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollegeExamen
{
  public class MobileController
  {
    private Mobile model;
    private MobileUI view;

    private List<GameDemoController> gameDemoControllers = new List<GameDemoController>();

    public MobileController(Mobile mModel)
    {
      model = mModel;
    }

    public GameDemoUI GetGameDemoUI(int index) 
    {
      GameDemo g = model.GetGameDemo(index);
      gameDemoControllers.Add(new GameDemoController(g));
      return gameDemoControllers[index].GetGameDemoUI();
    } 

    public MobileUI GetMobileUI()
    {
      this.view = new MobileUI(model, this);
      return view;
    }
  }
}
