﻿namespace GameCollegeExamen
{
  partial class MobileUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblStemmen2 = new System.Windows.Forms.Label();
      this.lblAantalstemmen = new System.Windows.Forms.Label();
      this.lblStemmen1 = new System.Windows.Forms.Label();
      this.lblBezoeker = new System.Windows.Forms.Label();
      this.lblWelkom = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // lblStemmen2
      // 
      this.lblStemmen2.AutoSize = true;
      this.lblStemmen2.Location = new System.Drawing.Point(293, 0);
      this.lblStemmen2.Name = "lblStemmen2";
      this.lblStemmen2.Size = new System.Drawing.Size(76, 13);
      this.lblStemmen2.TabIndex = 9;
      this.lblStemmen2.Text = "keer stemmen.";
      // 
      // lblAantalstemmen
      // 
      this.lblAantalstemmen.AutoSize = true;
      this.lblAantalstemmen.Location = new System.Drawing.Point(284, 0);
      this.lblAantalstemmen.Name = "lblAantalstemmen";
      this.lblAantalstemmen.Size = new System.Drawing.Size(13, 13);
      this.lblAantalstemmen.TabIndex = 8;
      this.lblAantalstemmen.Text = "3";
      // 
      // lblStemmen1
      // 
      this.lblStemmen1.AutoSize = true;
      this.lblStemmen1.Location = new System.Drawing.Point(223, 0);
      this.lblStemmen1.Name = "lblStemmen1";
      this.lblStemmen1.Size = new System.Drawing.Size(64, 13);
      this.lblStemmen1.TabIndex = 7;
      this.lblStemmen1.Text = "U mag maar";
      // 
      // lblBezoeker
      // 
      this.lblBezoeker.AutoSize = true;
      this.lblBezoeker.Location = new System.Drawing.Point(47, 0);
      this.lblBezoeker.Name = "lblBezoeker";
      this.lblBezoeker.Size = new System.Drawing.Size(52, 13);
      this.lblBezoeker.TabIndex = 6;
      this.lblBezoeker.Text = "Bezoeker";
      // 
      // lblWelkom
      // 
      this.lblWelkom.AutoSize = true;
      this.lblWelkom.Location = new System.Drawing.Point(3, 0);
      this.lblWelkom.Name = "lblWelkom";
      this.lblWelkom.Size = new System.Drawing.Size(49, 13);
      this.lblWelkom.TabIndex = 5;
      this.lblWelkom.Text = "Welkom,";
      // 
      // MobileUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblStemmen2);
      this.Controls.Add(this.lblAantalstemmen);
      this.Controls.Add(this.lblStemmen1);
      this.Controls.Add(this.lblBezoeker);
      this.Controls.Add(this.lblWelkom);
      this.Name = "MobileUI";
      this.Size = new System.Drawing.Size(857, 590);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblStemmen2;
    private System.Windows.Forms.Label lblAantalstemmen;
    private System.Windows.Forms.Label lblStemmen1;
    private System.Windows.Forms.Label lblBezoeker;
    private System.Windows.Forms.Label lblWelkom;
  }
}
