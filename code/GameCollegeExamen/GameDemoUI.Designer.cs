﻿namespace GameCollegeExamen
{
  partial class GameDemoUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnDemo = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnDemo
      // 
      this.btnDemo.BackColor = System.Drawing.Color.DarkGray;
      this.btnDemo.Location = new System.Drawing.Point(3, 3);
      this.btnDemo.Name = "btnDemo";
      this.btnDemo.Size = new System.Drawing.Size(137, 49);
      this.btnDemo.TabIndex = 1;
      this.btnDemo.Text = "GameDemo";
      this.btnDemo.UseVisualStyleBackColor = false;
      this.btnDemo.Click += new System.EventHandler(this.btnDemo_Click);
      // 
      // GameDemoUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnDemo);
      this.Name = "GameDemoUI";
      this.Size = new System.Drawing.Size(145, 672);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnDemo;
  }
}
